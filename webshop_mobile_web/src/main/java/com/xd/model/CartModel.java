package com.xd.model;

import lombok.Data;
import xd.product.api.entity.ProductInfo;

import java.io.Serializable;

/**
 * 购物车模型类
 */
@Data
public class CartModel implements Serializable {
    private ProductInfo product;
    private Integer count;
}
