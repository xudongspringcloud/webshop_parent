package com.xd.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Administrator on 2020/2/21.
 */
@Getter
@Setter
public class RegistDto {
    private String username;
    private String password;
    private String email;
}
