package com.xd.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import xd.member.api.service.UserApiService;

/**
 * Created by Administrator on 2020/2/21.
 */
@FeignClient("member")
public interface MemberApiServiceFeign extends UserApiService {
}
