package com.xd.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import xd.product.api.service.ProductApiService;

/**
 * Created by Administrator on 2020/2/28.
 */
@FeignClient("product")
public interface ProductApiServiceFeign extends ProductApiService {
}
