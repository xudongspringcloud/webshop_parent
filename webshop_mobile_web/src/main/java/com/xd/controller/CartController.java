package com.xd.controller;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xd.feign.ProductApiServiceFeign;
import com.xd.model.CartModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;
import xd.common.base.ResponseInfo;
import xd.common.utils.CookieUtil;
import xd.product.api.entity.ProductInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 购物车
 */
@Controller
public class CartController {
    private static final String CART = "cart"; // 购物车页面
    @Autowired
    Jedis jedis;

    @Autowired
    ProductApiServiceFeign productApiServiceFeign;

    @PostMapping("/addCart")
    @ResponseBody
    public String addCart(HttpServletRequest request, HttpServletResponse response){

        String  id= request.getParameter("id");
        String token = CookieUtil.getUid(request,"token");
        String userStr = jedis.get(token);
        if(userStr==null){// 1未登录，将数据放入cookie
            String cartStr = CookieUtil.getUid(request,"cartList");
            CookieUtil.addCookie(response,"cartList", URLEncoder.encode(doAddCart(id,cartStr)),60*60*24*90);
        }else{// 2 已登录，放入redis
            JSONObject user =  JSONObject.parseObject(userStr);
            String loginId = user.getString("loginId");
            String key = loginId+"_cartList";
            String cartStr = jedis.get(key);
            jedis.set(key,doAddCart(id,cartStr));
        }
        return "success";
    }
    public String doAddCart(String id,String cartStr){
        JSONObject cartJson = null;
        if(cartStr==null){ // 如果没有，就创建对象
            cartJson = new JSONObject();
            cartJson.put(id,1);
        }else{
            cartStr = URLDecoder.decode(cartStr);
            cartJson = JSONObject.parseObject(cartStr);
            cartJson.put(id,cartJson.getIntValue(id)+1);
        }
        return cartJson.toString();
    }
    @GetMapping("/cartPage")
    public String cartPage(HttpServletRequest request, Model model){
        String token = CookieUtil.getUid(request,"token");
        String userStr = jedis.get(token);
        ResponseInfo<List<CartModel>> info = new ResponseInfo<>();
        String cartStr = null;
        if(userStr==null){ // 未登录，从cookie 取值
            cartStr = CookieUtil.getUid(request,"cartList");
        }else{
            JSONObject user =  JSONObject.parseObject(userStr);
            String loginId = user.getString("loginId");
            String key = loginId+"_cartList";
            cartStr = jedis.get(key);
        }
        cartStr = URLDecoder.decode(cartStr);
        JSONObject jsonObject = JSONObject.parseObject(cartStr);
        List<CartModel> list = new ArrayList<>();
        for (String id:jsonObject.keySet()) {
            ResponseInfo<ProductInfo> responseInfo = productApiServiceFeign.getProductById(Long.valueOf(id));
            ObjectMapper mapper = new ObjectMapper();
            ProductInfo productInfo = mapper.convertValue(responseInfo.getData(), ProductInfo.class);// rpc 调用返回的 map，这里转成需要的对象
            CartModel cartModel = new CartModel();
            cartModel.setProduct(productInfo);
            cartModel.setCount(jsonObject.getInteger(id));
            list.add(cartModel);
        }
        if(list.size()==0) {
            info.setCode(999);
        }else {
            info.setCode(200);
            info.setData(list);
        }
        model.addAttribute("result",info);
        return CART;
    }
    @ResponseBody
    @DeleteMapping("/delCartById")
    public String delCartById(HttpServletRequest request, Model model){
        return null;
    }

}
