package com.xd.controller;

import com.alibaba.fastjson.JSONObject;
import com.xd.dto.RegistDto;
import com.xd.feign.MemberApiServiceFeign;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import redis.clients.jedis.Jedis;
import xd.common.base.ResponseInfo;
import xd.common.constants.ResponseInfoConstants;
import xd.common.utils.CookieUtil;
import xd.member.api.dto.UserLoginDto;
import xd.member.api.dto.UserOutDto;
import xd.member.api.dto.UserRegistDto;
import xd.member.api.entity.UserEntity;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2020/2/21.
 */
@SuppressWarnings("SpringJavaAutowiringInspection")
@Controller
@Slf4j
public class UserController {
    private static final String LOCALREGIST = "localRegist"; // 注册页面
    private static final String INDEX = "index"; // 首页
    private static final String LOGIN = "login"; // 登录页
    private static final String USER = "user";
    @Autowired
    private MemberApiServiceFeign memberApiServiceFeign;
    @Autowired
    Jedis jedis;
    @GetMapping("/registPage")
    public String regist(){
        return LOCALREGIST;
    }
    @GetMapping("/loginPage")
    public String login(){
        return LOGIN;
    }
    @PostMapping(value="/regist")
    public String regist(UserRegistDto userEntity, HttpServletRequest request){
        // 1. 验证参数
        // 2. 调用会员注册接口
        ResponseInfo responseInfo = memberApiServiceFeign.regist(userEntity);
        // 3. 如果失败，跳转到失败页面
        if(!responseInfo.getCode().equals(ResponseInfoConstants.HTTP_SUCCESS_CODE)){
            request.setAttribute("error", responseInfo.getMsg());
            return LOCALREGIST;
        }
        // 4. 如果成功,跳转到成功页面
        return LOGIN;
    }
    @PostMapping("login")
    public String login(UserLoginDto userEntity, HttpServletRequest request, HttpServletResponse response){
        log.info(userEntity.getPassword()+"  "+userEntity.getLoginId());
        ResponseInfo<String> info = memberApiServiceFeign.login(userEntity);
        // 1.验证参数
        // 2.调用登录接口，获取token信息
        if (!info.getCode().equals(ResponseInfoConstants.HTTP_SUCCESS_CODE)) {
            request.setAttribute("error", "账号或者密码错误!");
            return LOGIN;
        }
        String tokenValue = info.getData();
        CookieUtil.addCookie(response, "token", tokenValue, 1000*60*60); // 毫秒 1小时
        return INDEX;
    }
    @GetMapping("userPage")
    public String userPage(HttpServletRequest request,Model model){
        ResponseInfo<JSONObject> info = new ResponseInfo<>();
        String value = CookieUtil.getUid(request,"token");
        String jsonstr = jedis.get(value);
        if(jsonstr==null){
            log.info("登录已失效");
            info.setCode(999);
        }else{
            JSONObject user =  JSONObject.parseObject(jsonstr);
            info.setCode(200);
            info.setData(user);
        }
        model.addAttribute("result",info);
        return USER;
    }
    @GetMapping("loginOut")
    public String loginOut(HttpServletRequest request){
        String token = CookieUtil.getUid(request,"token");
        UserOutDto userOutDto = new UserOutDto();
        userOutDto.setToken(token);
        ResponseInfo<Long> info = memberApiServiceFeign.loginOut(userOutDto);
        log.info("退出成功");
        return INDEX;
    }

}
