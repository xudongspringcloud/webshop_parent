package com.xd.controller;

import com.xd.feign.ProductApiServiceFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xd.common.base.ResponseInfo;
import xd.product.api.entity.ProductCategory;
import xd.product.api.entity.ProductInfo;

import java.util.List;

/**
 * Created by Administrator on 2020/2/28.
 */
@SuppressWarnings("SpringJavaAutowiringInspection")
@Controller
public class ProductController {
    private static final String CATEGORY = "category"; // 大类页面
    private static final String LIST = "list"; // 商品列表页面
    @Autowired
    ProductApiServiceFeign productApiServiceFeign;

    /**
     * 商品分类页面
     * @param model
     * @return
     */
    @GetMapping("/categoryPage")
    public String categoryPage(Model model){
        ResponseInfo<List<ProductCategory>> responseInfo = productApiServiceFeign.getProductCategoryList();
        model.addAttribute("result",responseInfo);
        return CATEGORY;
    }

    /**
     * 商品列表页面
     * @return
     */
    @GetMapping("/listPage")
    public String listPage(Model model,@RequestParam("id") Long id,@RequestParam("cateName") String cateName){
        ResponseInfo<List<ProductInfo>> responseInfo = productApiServiceFeign.getProductListByCategory( id);
        model.addAttribute("result",responseInfo);
        model.addAttribute("cateName",cateName);
        return LIST;
    }
}
