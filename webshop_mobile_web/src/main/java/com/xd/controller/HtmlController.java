package com.xd.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Created by Administrator on 2020/2/28.
 */
@Controller
public class HtmlController {
    private static final String SEARCH = "category"; // 注册页面
    private static final String DOWNLOAD = "download"; // 下载页面



    private static final String GOODS = "goods"; // 大类页面


    @GetMapping("/searchPage")
    public String searchPage(){
        return SEARCH;
    }
    @GetMapping("/downloadPage")
    public String downloadPage(){
        return DOWNLOAD;
    }


    @GetMapping("/goodsPage")
    public String goodsPage(){
        return GOODS;
    }
    @GetMapping("/orderPage")
    public String orderPage(){
        return "order";
    }
    @GetMapping("/order_waitPage")
    public String order_waitPage(){
        return "order_wait";
    }
    @GetMapping("/order_logisticsPage")
    public String order_logisticsPage(){
        return "order_logistics";
    }
    @GetMapping("/order_rechargePage")
    public String order_rechargePage(){
        return "order_recharge";
    }
    @GetMapping("/couponPage")
    public String couponPage(){
        return "coupon";
    }
    @GetMapping("/addressPage")
    public String addressPage(){
        return "address";
    }
    @GetMapping("/servicePage")
    public String servicePage(){
        return "service";
    }
    @GetMapping("/commentPage")
    public String commentPage(){
        return "comment";
    }

}
