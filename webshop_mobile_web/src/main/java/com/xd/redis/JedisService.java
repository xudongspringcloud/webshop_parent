package com.xd.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;

/**
 * Created by Administrator on 2020/2/25.
 */
@Configuration
public class JedisService {
    @Value("${redis.host}")
    private String host;
    @Value("${redis.port}")
    private Integer port;
    @Value("${redis.timeout}")
    private Integer timeout;
    @Bean
    public Jedis getJedis(){
        Jedis jedis = new Jedis(host,port,timeout);
        return jedis;
    }
}
