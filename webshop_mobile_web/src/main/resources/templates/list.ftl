<!DOCTYPE html>
<!-- saved from url=(0055)http://m.mi.com/index.html#ac=product&op=list&cat_id=-1 -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="author" content="xiaomi">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon-precomposed" href="http://img01.mifile.cn/m/app/touch-icon.png">
    <link rel="shortcut icon" href="http://m.mi.com/favicon.ico" type="image/x-icon">
    <meta name="description" content="手版小米商城客户端可以随时随地参与预约抢购小米手机，轻松下单购买，实时查询订单信息掌握物流状态，小米商城app手机客户端还有新品信息通知推送功能，所有商品信息随身掌握。">
    <meta name="keywords" content="小米商城,小米手机,小米商城下载,小米商城app">
    <link rel="stylesheet" type="text/css" href="css/order.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>
    <title>小米商城-小米官网</title>
    <style type="text/css">
        object,embed{  -webkit-animation-duration:.001s;-webkit-animation-name:playerInserted;
                        -ms-animation-duration:.001s;-ms-animation-name:playerInserted;
                        -o-animation-duration:.001s;-o-animation-name:playerInserted;
                        animation-duration:.001s;animation-name:playerInserted;
                    }
        @-webkit-keyframes playerInserted{from{opacity:0.99;}to{opacity:1;}}
        @-ms-keyframes playerInserted{from{opacity:0.99;}to{opacity:1;}}
        @-o-keyframes playerInserted{from{opacity:0.99;}to{opacity:1;}}
        @keyframes playerInserted{from{opacity:0.99;}to{opacity:1;}}
        .circle {
            width: 30px;
            height: 30px;
            border-radius: 15px;
            border:1px solid;
            background-color:red;
            text-align:center;
        }

    </style>
    <script type="text/javascript">
        function addCart(id) {
            var data = {id:id};
            $.ajax({
                type:"POST",
                url:"addCart",
                data:data,
                dateType: "json",
                success:function () {
                    alert("加入购物车成功");
                    $('#circle').text("12");
                }
            })
        }
    </script>
</head>
<body style="display: block; background: rgb(235, 236, 237);">
<div id="Cheader" style="">
    <div id="header" class="header_03">
        <div class="back">
            <a href="/mobile" class="arrow" data-stat-id="ee3d8ad4696c648d" onclick="_msq.push([&#39;trackEvent&#39;, &#39;ac432efee4084034-ee3d8ad4696c648d&#39;, &#39;/home/&#39;, &#39;pcpid&#39;]);">首页</a>
        </div>
        <div class="tit" style="">
            <h3>${cateName!}</h3>
        </div>
        <div class="nav">
            <ul>
                <li class="cart">
                    <span class="circle" id="cartNum"> 12 </span>
                    <a href="http://m.mi.com/index.html#ac=shopping&op=index" data-stat-id="96c452cb9824c0df" onclick="_msq.push([&#39;trackEvent&#39;, &#39;ac432efee4084034-96c452cb9824c0df&#39;, &#39;/index.html#ac=shopping&amp;op=index&#39;, &#39;pcpid&#39;]);">购物车</a>
                    <span id="ShoppingCartNum" style="display:none"></span>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="wrapper" class="xm_app">
    <div id="viewport" class="viewport" style="background: transparent;">
        <div id="crumbs" class="crumbs" style="display: none;">
            <ul>
                <li>
                    <a href="http://m.mi.com/home/" data-stat-id="d4dd3d65fbbaad6b" onclick="_msq.push([&#39;trackEvent&#39;, &#39;ac432efee4084034-d4dd3d65fbbaad6b&#39;, &#39;/home/&#39;, &#39;pcpid&#39;]);"><span>首页</span></a>
                </li>
                <li><a href="http://m.mi.com/index.html#ac=product&op=category" data-stat-id="d98d92fcc3446200" onclick="_msq.push([&#39;trackEvent&#39;, &#39;ac432efee4084034-d98d92fcc3446200&#39;, &#39;/index.html#ac=product&amp;op=category&#39;, &#39;pcpid&#39;]);">分类</a>
                </li>
                <li><a href="javascript:;" data-stat-id="1e97e999375125bb" onclick="_msq.push([&#39;trackEvent&#39;, &#39;ac432efee4084034-1e97e999375125bb&#39;, &#39;javascript:;&#39;, &#39;pcpid&#39;]);">小米手机</a>
                </li>
            </ul>
        </div>
        <div class="newList_view">
            <#if result.code==200>
                <#list result.data as p>
                    <div class="nlb">
                        <a href="goodsPage?id=${p.id?c}" class="b b1" data-stat-id="b654bbfaa0fce48e">
                            <div class="b11">
                                <div class="imgurl">
                                    <img src="${p.image!}">
                                </div>
                            </div>
                            <div class="b12">
                                <div class="name">
                                    <p><span>${p.title!}</span><strong>¥${p.price!}元起</strong></p>
                                </div>
                                <div class="brief">
                                    <p>${p.sellPoint!}</p>
                                </div>
                                <div class="tips">
                                    <p></p>
                                    <p></p>
                                </div>
                            </div>
                        </a>
                        <div class="b b2">
                            <div class="b21">
                                <a onclick="addCart(${p.id?c})">加入购物车</a>
                            </div>
                            <div class="b21">
                                <a href="http://s1.mi.com/m/1/m/presell/version/index.html?phone_type=minote">立即购买</a>
                            </div>
                        </div>
                    </div>
                </#list>
            </#if>
        </div>
    </div>
</div>
<div id="maskLoad" class="maskLoad" style="display:none;"><span>正在加载...</span></div>
<div id="mask" class="mask hide"></div>
<div class="pop_panel hide" id="pop_panel">
    <div class="content">
        <div class="bd giftCard_view">
            <div class="gfc5">
                <div class="b b1">
                    <p>提示</p>
                </div>
                <div class="b b2">
                    <p id="content"></p>
                </div>
            </div>
        </div>
    </div><a href="javascript:;" id="CloseMaskBtn" class="close" data-stat-id="2c8d124be55949cc" onclick="_msq.push([&#39;trackEvent&#39;, &#39;ac432efee4084034-2c8d124be55949cc&#39;, &#39;javascript:;&#39;, &#39;pcpid&#39;]);"><img src="images/close.png"></a>
</div>
<div id="popup" style="display:none"><p></p></div>
<div class="loading_view" style="visibility:hidden">
    <div class="loading_wrap">
        <div class="loading_msg">正在加载 ...</div><a href="javascript:;" class="close" onclick="_msq.push([&#39;trackEvent&#39;, &#39;ac432efee4084034-22892765a2389a00&#39;, &#39;javascript:;&#39;, &#39;pcpid&#39;]);MipuUtil.MaskLoading.close();" data-stat-id="22892765a2389a00"><img src="images/loadingClose.png"></a>
    </div>
</div>

</body></html>