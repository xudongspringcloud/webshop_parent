<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- 屏幕宽度 && 放大比例 && minimal-ui Safari 网页加载时隐藏地址栏与导航栏-->
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-
	scale=1,maximum-scale=1,user-scalable=0,minimal-ui">
    <!-- safari私有meta标签，允许全屏模式浏览 -->
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <!-- ios系统的私有标签，它指定在web app状态下，ios设备中顶端的状态条的颜色 -->
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <!-- 设备浏览网页时对数字不启用电话功能 -->
    <meta content="telephone=no,email=no" name="format-detection" />
    <title>Jiuchacha - Cart</title>
    <link rel="stylesheet" href="css/common.css">
    <!-- 自适应样式单 -->
    <link rel="stylesheet" href="css/adaptive.css">
    <link rel="stylesheet" href="css/cart.css">
    <style type="text/css">
        table{
            width:90%;
            margin:auto;
        }
        table tr td{
            height:40px;
        }
        img{
            height:80px;
        }
    </style>
    <script type="application/javascript">

    </script>
</head>
<body>
<div id="wrapper">
    <div class="page-shopping" data-log="购物车">
        <div class="header">

            <div class="left">
                <a href="index.html" title="酒查查商城" data-log="HEAD-首页" class="home"><!--vue-if--><span class="icon-home"></span></a>
            </div>

            <div class="tit">
                <h2 data-log="HEAD-标题-购物车">
                    <span class="title">购物车</span>
                </h2>
            </div>

            <div class="right">
                <ul>
                    <li class="cart">
                        <a href="#" title="购物车" data-log="HEAD-购物车"><span class="icon icon-gouwuche"></span></a>
                    </li>
                </ul>
            </div>

        </div>
        <div>
            <div class="list_order list_order_tmp list_nav">
                    <#if result.code==200>
                        <table>
                        <#list result.data as p>
                            <tr>
                                <td><input type="checkbox" ></td>
                                <td>
                                    <div class="imgurl">
                                        <img src="${p.product.image!}">
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <p><span>${p.product.title!}</span></p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <p><strong>¥${p.product.price!}元</strong></p>
                                        <p><strong> 数量：${p.count}</strong></p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <a href="">删除</a>
                                    </div>
                                </td>
                            </tr>
                        </#list>
                        </table>
                    <#else >
                        <div class="shopping-empty">
                            <div class="tips_msg">
                                <img src="images/cart.png" class="img"><h3>购物车还是空的</h3>
                                <p>现在就去选购吧</p>
                            </div>
                            <div class="tips_btn">
                                <a href="#/product/category" class="xm-button xm-button-gray"><span>去逛逛</span></a>
                            </div>
                        </div>
                    </#if>
            </div>
        </div>
    </div>
</div>
</body>
</html>