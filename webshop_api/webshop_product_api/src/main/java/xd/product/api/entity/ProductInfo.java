package xd.product.api.entity;

import lombok.Getter;
import lombok.Setter;
import xd.common.base.BaseEntity;

/**
 * Created by Administrator on 2020/2/26.
 */
@Getter
@Setter
public class ProductInfo extends BaseEntity {
    public String title; // 商品名称
    public String sellPoint; // 商品卖点
    public Double price; // 商品价格
    public Long num; // 库存
    public String barcode; // 条码
    public String image; // 商品图片
    public Long parentId; // 父类别
    public Long cid; // 类别
    public Integer status; // 状态 1-正常，2-下架，3-删除
}
