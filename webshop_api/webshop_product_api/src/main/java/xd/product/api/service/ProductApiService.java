package xd.product.api.service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import xd.common.base.ResponseInfo;
import xd.product.api.entity.ProductCategory;
import xd.product.api.entity.ProductDetails;
import xd.product.api.entity.ProductInfo;

/**
 * Created by Administrator on 2020/2/26.
 */
@Api(value="ProductApiService",description = "商品操作相关接口")
public interface ProductApiService {
    /**
     * 根据商品分类查询商品列表
     * @param
     * @return
     */
    @ApiOperation(value = "根据商品分类查询商品列表",notes = "data ：{}")
    @GetMapping(value="productList/{catagory}",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo getProductListByCategory(@PathVariable("catagory")  Long catagory);

    /**
     * 根据商品id,获取商品信息
     * @param id
     * @return
     */
    @ApiOperation(value = "根据商品id,获取商品信息",notes = "data ：{}")
    @GetMapping(value="product/{id}",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo getProductById(@PathVariable("id")  Long id);

    /**
     * 新增商品信息
     * @param productInfo
     * @return
     */
    @ApiOperation(value = "新增商品信息",notes = "data ：{}")
    @PostMapping(value="addproduct",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo addProduct(@RequestBody ProductInfo productInfo);

    /**
     * 获取商品分类列表
     * @return
     */
    @ApiOperation(value = "获取商品分类列表",notes = "data ：{}")
    @GetMapping(value="productCategoryList",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo getProductCategoryList();

    /**
     * 增加商品分类
     * @param categoryEntity
     * @return
     */
    @ApiOperation(value = "增加商品分类",notes = "data ：{}")
    @PostMapping(value="addProductCategory",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo addProductCategory(@RequestBody ProductCategory categoryEntity);

    /**
     * 获取商品详情描述
     * @param id
     * @return
     */
    @ApiOperation(value = "获取商品详情描述",notes = "data ：{}")
    @GetMapping(value="productDesc/{id}",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo getProductDesc(@PathVariable("id")  Long id);

    /**
     * 添加商品详情描述
     * @param descEntity
     * @return
     */
    @ApiOperation(value = "添加商品详情描述",notes = "data ：{}")
    @PostMapping(value="addProductDesc",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo addProductDesc(@RequestBody ProductDetails descEntity);

}
