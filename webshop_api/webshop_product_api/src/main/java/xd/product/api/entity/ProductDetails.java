package xd.product.api.entity;

import lombok.Getter;
import lombok.Setter;
import xd.common.base.BaseEntity;

/**
 * Created by Administrator on 2020/2/26.
 */
@Getter
@Setter
public class ProductDetails extends BaseEntity {
    public String itemDesc;// 商品描述
}
