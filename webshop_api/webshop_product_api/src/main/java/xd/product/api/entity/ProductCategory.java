package xd.product.api.entity;

import lombok.Getter;
import lombok.Setter;
import xd.common.base.BaseEntity;

/**
 * Created by Administrator on 2020/2/26.
 */
@Getter
@Setter
public class ProductCategory extends BaseEntity {

    public Long parentId;
    public String name; // 类别名称
    public String img;//图片地址
    public int status; // 状态。可选值:1(正常),2(删除)
    public int sortOrder; //排列序号，表示同级类目的展现次序，如数值相等则按名称次序排列。取值范围:大于零的整数
    public int isParent;//该类目是否为父类目，1为true，0为false


}
