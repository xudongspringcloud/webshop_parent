package xd.member.api.service;

import org.springframework.web.bind.annotation.ResponseBody;
import xd.common.base.ResponseInfo;
import xd.member.api.dto.UserLoginDto;
import xd.member.api.dto.UserOutDto;
import xd.member.api.dto.UserRegistDto;
import xd.member.api.entity.UserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("member")
@Api(value="UserApiService",description = "用户操作相关接口")
public interface UserApiService {
    /**
     *  1 用户注册
     * @param user
     * @return
     */
    @ApiOperation(value = "用户注册",notes = "data ：{}")
    @PostMapping(value="/regist",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo regist(@RequestBody UserRegistDto user);
    /**
     *  2 用户登录
     * @param user
     * @return
     */
    @ApiOperation(value = "用户登录",notes = "data ：{}")
    @PostMapping(value="/login",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo login(@RequestBody UserLoginDto user);
    /**
     *  2 登录退出
     * @return
     */
    @ApiOperation(value = "登录退出",notes = "data ：{}")
    @PostMapping(value="/loginOut",produces = {"application/json;charset=UTF-8"})
    public ResponseInfo loginOut(@RequestBody UserOutDto token);
}
