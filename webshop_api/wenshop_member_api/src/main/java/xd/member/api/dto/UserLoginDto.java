package xd.member.api.dto;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class UserLoginDto {
    @ApiParam(value = "loginId",required = true)
    private String loginId;
    @ApiParam(value = "password",required = true)
    private String password;

}
