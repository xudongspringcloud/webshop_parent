package xd.member.api.dto;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class UserOutDto {
    @ApiParam(value = "token",required = true)
    private String token;
}
