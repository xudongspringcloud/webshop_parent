package xd.member.api.entity;

import lombok.Data;
import lombok.ToString;
import xd.common.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Administrator on 2020/2/13.
 */
@Data
@ToString(callSuper = true,includeFieldNames=true)
public class UserEntity extends BaseEntity {
    private String username;
    private String loginId;
    private String password;
    private String phone;
    private String email;

}
