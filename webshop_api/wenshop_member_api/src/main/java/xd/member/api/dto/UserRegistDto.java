package xd.member.api.dto;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanCopier;
import xd.common.utils.DateUtils;
import xd.member.api.entity.UserEntity;

import java.io.Serializable;

@Data
public class UserRegistDto implements Serializable {
    @ApiParam(value = "userName",required = true)
    private String userName;
    @ApiParam(value = "loginId",required = true)
    private String loginId;
    @ApiParam(value = "password",required = true)
    private String password;
    @ApiParam(value = "phone",required = true)
    private String phone;
    @ApiParam(value = "email",required = true)
    private String email;

    public static void main(String[] args) {
        UserRegistDto dto = new UserRegistDto();
        dto.setEmail("11");
        dto.setLoginId("dddd");
        dto.setUserName("dddddddd");
        UserEntity entity = new UserEntity();
        long start = System.currentTimeMillis();


        for (int i = 0; i < 10000000; i++) {
            BeanCopier beanCopier = BeanCopier.create(UserRegistDto.class,UserEntity.class,false);
            // BeanUtils.copyProperties(dto,entity);
            beanCopier.copy(dto,entity,null);
        }
        long end = System.currentTimeMillis();
        System.out.println(end-start);
        System.out.println(dto);
        System.out.println(entity);


    }
}
