package xd.common.base;

import xd.common.constants.ResponseInfoConstants;

public class ResponseUtil {
    /**
     * 成功返回，带data数据
     * @param data
     * @return
     */
    public ResponseInfo setResultSuccessData(Object data) {
        return setBaseApiResponse(ResponseInfoConstants.HTTP_SUCCESS_CODE, ResponseInfoConstants.HTTP_SUCCESS_NAME, data);
    }
    /**
     *
     * @methodDesc: 返回 success ,不带数据
     * @param: @param
     *             msg
     * @param: @return
     */
    public ResponseInfo setResultSuccessData() {
        return setBaseApiResponse(ResponseInfoConstants.HTTP_SUCCESS_CODE, ResponseInfoConstants.HTTP_SUCCESS_NAME, null);
    }

    /**
     *
     * @methodDesc: 接口调用失败
     * @param: @param
     *             msg
     * @param: @return
     */
    public ResponseInfo setResultFail() {
        return setBaseApiResponse(ResponseInfoConstants.HTTP_FAIL_CODE ,ResponseInfoConstants.HTTP_FAIL_NAME,null);
    }

    /**



    /**
     *
     * @methodDesc: 自定义错误返回msg
     * @param: @param
     *             msg
     * @param: @return
     */
    public ResponseInfo setResultFail(String msg) {
        return setBaseApiResponse(ResponseInfoConstants.HTTP_FAIL_CODE, msg, null);
    }

    /**
     *
     * @methodDesc: 自定义返回结果
     * @param: @param
     *             code
     * @param: @param
     *             msg
     * @param: @param
     *             data
     * @param: @return
     */
    public ResponseInfo setBaseApiResponse(Integer code, String msg, Object data) {
        ResponseInfo baseApiResponse = new ResponseInfo();
        baseApiResponse.setCode(code);
        baseApiResponse.setMsg(msg);
        if (data != null)
            baseApiResponse.setData(data);
        return baseApiResponse;
    }
}
