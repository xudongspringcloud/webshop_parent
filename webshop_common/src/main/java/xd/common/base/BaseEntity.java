package xd.common.base;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Administrator on 2020/2/13.
 */
@Data
public class BaseEntity implements Serializable {
    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Timestamp createtime;
    /**
     * 修改时间
     */
    private Timestamp updatetime;


}
