
package xd.common.base;

import lombok.Getter;
import lombok.Setter;


/**
 * 返回结果的方法封装
 */
@Getter
@Setter
public class ResponseInfo<T>{
	// 响应code
	private Integer code;
	// 消息内容
	private String msg;
	// 返回data
	private T data;

	public static void main(String[] args) {

		System.out.println(new ResponseInfo<String>());
	}
}
