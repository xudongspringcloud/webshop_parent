
package xd.common.constants;

public interface Constants {
	//用户会话保存90天
	Long USER_TOKEN_TIMETOLIVE = 60 * 60 * 24 * 90l;
	int WEBUSER_COOKIE_TOKEN_TERMVALIDITY = 1000*60 * 60 * 24 * 90;
	String USER_TOKEN = "token";
}
