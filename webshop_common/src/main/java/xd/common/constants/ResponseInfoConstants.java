
package xd.common.constants;

public interface ResponseInfoConstants {
	public static final String HTTP_SUCCESS_NAME="success";
	public static final String HTTP_FAIL_NAME="fail";
	public static final Integer HTTP_SUCCESS_CODE = 200;
	public static final Integer HTTP_FAIL_CODE = 999;
}
