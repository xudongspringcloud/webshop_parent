package xd.mybatis.base.util;




import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReflectUtils {
    /**
     * 获取 子类和父类的所有属性数字，排除 id 属性
     * @param obj
     * @return
     */
    public static Field[] getFileds(Object obj){
        Class aClass=obj.getClass();
        Field[] fields= aClass.getDeclaredFields();
        Field[] superFields = aClass.getSuperclass().getDeclaredFields();
        Stream<Field> fieldStream = Stream.concat(Stream.of(fields),Stream.of(superFields));
        return fieldStream.filter(m->!"id".equals(m.getName())).toArray(Field[]::new);
    }

    /**
     * 将获取的属性 转为 字符串
     * @param array
     * @return
     */
    public static String getStringColumns(Field [] array){
        return Stream.of(array).map(m->m.getName()).collect(Collectors.joining(","));
    }

    /**
     *  获取 值
     * @param array
     * @param obj
     * @return
     */
    public static String getStringValues(Field [] array,Object obj){
        Object[] strArray = Stream.of(array).map(m->setValue(m,obj)).toArray(Object[]::new);
        StringBuffer sbf = new StringBuffer();
        for (int i = 0; i < strArray.length; i++) {
            sbf.append(strArray[i]+",");
        }
        return sbf.toString().substring(0,sbf.length()-1);
    }

    public static Object setValue(Field field,Object obj){
        Object value=null;
        try {
            field.setAccessible(true);
            value = field.get(obj);
            if(value==null)
                return value;
            if (value instanceof String || value instanceof Timestamp)
                value = "'"+value+"'";
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return value;
    }


}
