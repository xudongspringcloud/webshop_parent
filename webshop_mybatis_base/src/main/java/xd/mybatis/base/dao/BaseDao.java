package xd.mybatis.base.dao;


import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
public interface BaseDao {
    /**
     * 新增 表数据
     * @param oj 实体类
     * @param table 表名
     * @return
     */
    @InsertProvider(type = BaseProvider.class,method = "save")
    public Integer save(@Param("obj") Object oj, @Param("table") String table);


}
