package xd.mybatis.base.dao;


import xd.mybatis.base.util.ReflectUtils;
import org.apache.ibatis.jdbc.SQL;

import java.lang.reflect.Field;
import java.util.Map;

public class BaseProvider {

    public String save(Map<String,Object> map){
        final Object obj= map.get("obj");
        final String table = (String) map.get("table");
        Class objClass = obj.getClass();
        objClass.getDeclaredFields();
        String sql = new SQL() {
            {
                INSERT_INTO(table);
                Field [] fields = ReflectUtils.getFileds(obj);
               String columns = ReflectUtils.getStringColumns(fields);
               String values = ReflectUtils.getStringValues(fields,obj);
                VALUES(columns, values);
            }
        }.toString();
        return sql;
    }

    public String update(Map<String,Object> map){
        final Object obj= map.get("obj");
        final String table = (String) map.get("table");
        Class objClass = obj.getClass();
        objClass.getDeclaredFields();
        String sql = new SQL() {
            {
                UPDATE(table);
                SET("");
                WHERE("");
                INSERT_INTO(table);
                Field[] fields = ReflectUtils.getFileds(obj);

            }
        }.toString();
        return sql;
    }

}
