package xd.product.doapi.contains;

public class TableContains {

    public final static String TABLE_PRODUCT_INFO  = "product_info";
    public final static String TABLE_PRODUCT_DESC  = "product_desc";
    public final static String TABLE_PRODUCT_CATEGORY  = "product_category";
}
