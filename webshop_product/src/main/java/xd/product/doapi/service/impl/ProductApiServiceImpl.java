package xd.product.doapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import xd.common.base.ResponseInfo;
import xd.common.base.ResponseUtil;
import xd.common.utils.DateUtils;
import xd.product.api.entity.ProductCategory;
import xd.product.api.entity.ProductDetails;
import xd.product.api.entity.ProductInfo;
import xd.product.api.service.ProductApiService;
import xd.product.doapi.contains.TableContains;
import xd.product.doapi.dao.ProductCategoryDao;
import xd.product.doapi.dao.ProductDao;
import xd.product.doapi.dao.ProductDescDao;

import java.util.List;

/**
 * Created by Administrator on 2020/2/26.
 */
@RestController
public class ProductApiServiceImpl extends ResponseUtil implements ProductApiService {
    @Autowired
    ProductDao productDao;
    @Autowired
    ProductDescDao productDescDao;
    @Autowired
    ProductCategoryDao productCategoryDao;
    @Override
    public ResponseInfo getProductListByCategory(Long catagory) {
        ResponseInfo<List<ProductInfo>> responseInfo = new ResponseInfo();
        try{
            List<ProductInfo> list = productDao.getProductListByCategory(catagory);
            responseInfo = setResultSuccessData(list);
        }catch (Exception e){
            responseInfo = setResultFail();
            e.printStackTrace();
        }
        return responseInfo;
    }

    @Override
    public ResponseInfo getProductById(Long id) {
        ResponseInfo<ProductInfo> responseInfo = new ResponseInfo();
        try{
            ProductInfo productInfo =  productDao.getProductById(id);
            responseInfo = setResultSuccessData(productInfo);
        }catch (Exception e){
            responseInfo = setResultFail();
            e.printStackTrace();
        }
        return responseInfo;
    }

    @Override
    public ResponseInfo addProduct(ProductInfo productInfo) {
        productInfo.setCreatetime(DateUtils.getTimestamp());
        productInfo.setUpdatetime(DateUtils.getTimestamp());
        Integer res = productDao.save(productInfo,TableContains.TABLE_PRODUCT_INFO);
        return setResultSuccessData(res);
    }

    @Override
    public ResponseInfo getProductCategoryList() {
        ResponseInfo<List<ProductCategory>> responseInfo = new ResponseInfo();
        try{
            List<ProductCategory> list = productCategoryDao.queryAll();
            responseInfo = setResultSuccessData(list);
        }catch (Exception e){
            responseInfo = setResultFail();
            e.printStackTrace();
        }

        return responseInfo;
    }

    @Override
    public ResponseInfo addProductCategory(ProductCategory categoryEntity) {
        categoryEntity.setCreatetime(DateUtils.getTimestamp());
        categoryEntity.setUpdatetime(DateUtils.getTimestamp());
        Integer res = productCategoryDao.save(categoryEntity, TableContains.TABLE_PRODUCT_CATEGORY);
        return setResultSuccessData(res);
    }

    @Override
    public ResponseInfo getProductDesc(Long id) {
        ResponseInfo<ProductDetails> responseInfo = new ResponseInfo();
        try{
            ProductDetails details =  productDescDao.getProductDesvById(id);
            responseInfo = setResultSuccessData(details);
        }catch (Exception e){
            responseInfo = setResultFail();
            e.printStackTrace();
        }
        return responseInfo;
    }

    @Override
    public ResponseInfo addProductDesc(ProductDetails descEntity) {
        descEntity.setCreatetime(DateUtils.getTimestamp());
        descEntity.setUpdatetime(DateUtils.getTimestamp());
        Integer res = productDao.save(descEntity,TableContains.TABLE_PRODUCT_DESC);
        return setResultSuccessData(res);
    }
}
