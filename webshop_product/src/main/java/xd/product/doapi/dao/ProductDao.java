package xd.product.doapi.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import xd.mybatis.base.dao.BaseDao;
import xd.product.api.entity.ProductInfo;

import java.util.List;

/**
 * Created by Administrator on 2020/2/26.
 */
@Mapper
public interface ProductDao extends BaseDao {
    @Select("select * from product_info where cid=#{category}")
    public List<ProductInfo> getProductListByCategory(Long category);
    @Select("select * from product_info where id=#{id}")
    public ProductInfo getProductById(Long id);
}
