package xd.product.doapi.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import xd.mybatis.base.dao.BaseDao;
import xd.product.api.entity.ProductCategory;

import java.util.List;

/**
 * Created by Administrator on 2020/2/26.
 */
@Mapper
public interface ProductCategoryDao extends BaseDao {
    @Select("select * from product_category  where isparent=1")
    public List<ProductCategory> queryAll();
}
