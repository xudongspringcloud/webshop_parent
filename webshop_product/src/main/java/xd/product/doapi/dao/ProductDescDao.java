package xd.product.doapi.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import xd.mybatis.base.dao.BaseDao;
import xd.product.api.entity.ProductDetails;

/**
 * Created by Administrator on 2020/2/26.
 */
@Mapper
public interface ProductDescDao extends BaseDao {
    @Select("select * from product_desc where id=#{id}")
    public ProductDetails getProductDesvById(Long id);
}
