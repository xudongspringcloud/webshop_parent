package xd.member.doapi;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Administrator on 2020/2/12.
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan("xd.member.doapi.dao")
@EnableApolloConfig
@EnableSwagger2
public class MemberServiceApplication {


    public static void main(String[] args) {
        SpringApplication.run(MemberServiceApplication.class,args);

    }
}
