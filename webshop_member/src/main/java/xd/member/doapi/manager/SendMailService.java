package xd.member.doapi.manager;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2020/2/19.
 */
public interface SendMailService {
    public void sendMail(JSONObject jsonObject);
}
