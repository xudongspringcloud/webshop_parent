package xd.member.doapi.manager.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;
import xd.member.doapi.manager.SendMailService;

/**
 * Created by Administrator on 2020/2/19.
 * 将 消息发送到 activemq 队列就行了
 */
@Service
@Slf4j
public class SendMailServiceImpl implements SendMailService {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Value("email_queue")
    private String queue;
    @Override
    public void sendMail(JSONObject jsonObject) {
        jmsMessagingTemplate.convertAndSend(queue,jsonObject.toJSONString());
        log.info("邮件发送加入消息队列成功，发送内容："+jsonObject.toJSONString());
    }
}
