package xd.member.doapi.manager;

import com.netflix.discovery.converters.Auto;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xd.common.utils.DateUtils;
import xd.member.doapi.dao.UserDao;
import xd.member.doapi.entity.UserEntity;
import xd.member.doapi.mq.templates.MailTemplates;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class XxlJobManager {
    @Autowired
    UserDao userDao;
    @Autowired
    SendMailService sendMailService;
    /**
     * 1、简单任务示例（Bean模式）
     *
     */
    @XxlJob("mailJobHandler")
    public ReturnT<String> demoJobHandler(String param) throws Exception {
        XxlJobLogger.log("XXL-JOB, mailJobHandler 发送邮件");
        String now = DateUtils.currentFormatDate("yyyy-MM-dd");
        List<UserEntity> entityList = userDao.findUserByBirthday(now);
        if (entityList!=null){
            for (UserEntity user:
                 entityList) {
                sendMailService.sendMail(MailTemplates.mailTemplates2(user.getEmail()));
            }
        }
        return new ReturnT<>("success");
    }
}
