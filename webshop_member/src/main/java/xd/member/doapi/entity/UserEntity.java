package xd.member.doapi.entity;

import lombok.Data;
import lombok.ToString;
import xd.common.base.BaseEntity;

/**
 * Created by Administrator on 2020/2/13.
 */
@Data
@ToString(callSuper = true,includeFieldNames=true)
public class UserEntity extends BaseEntity {
    private String username;
    private String loginId;
    private String password;
    private String phone;
    private String email;
    private String birthday;

}
