package xd.member.doapi.mq.templates;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2020/2/19.
 * 邮件发送模板
 */
public class MailTemplates {
    /**
     * 用户注册成功邮件
     * @return
     */
    public static JSONObject mailTemplates1(String address){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("head","mail");
        jsonObject.put("title","会员注册成功提醒，请查收");
        jsonObject.put("address",address);
        jsonObject.put("context","尊敬的用户，你注册会员已经成功");
        return jsonObject;
    }
    /**
     * 用户生日祝福
     * @return
     */
    public static JSONObject mailTemplates2(String address){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("head","mail");
        jsonObject.put("title","生日快乐");
        jsonObject.put("address",address);
        jsonObject.put("context","尊敬的用户，今天是你的生日，祝你生日快乐！");
        return jsonObject;
    }
}
