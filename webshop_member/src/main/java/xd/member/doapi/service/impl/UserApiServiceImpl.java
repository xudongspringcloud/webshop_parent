package xd.member.doapi.service.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.Jedis;
import xd.common.base.ResponseUtil;
import xd.common.base.ResponseInfo;
import xd.common.constants.Constants;
import xd.common.utils.MD5Util;
import xd.common.utils.TokenUtil;
import xd.member.api.dto.UserLoginDto;
import xd.member.api.dto.UserOutDto;
import xd.member.api.dto.UserRegistDto;
import xd.member.api.service.UserApiService;
import xd.member.doapi.contains.TableContains;
import xd.member.doapi.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import xd.member.doapi.entity.UserEntity;
import xd.member.doapi.manager.SendMailService;
import xd.member.doapi.mq.templates.MailTemplates;

@SuppressWarnings("ALL")
@RestController
@Slf4j
public class UserApiServiceImpl extends ResponseUtil implements UserApiService {
    @Autowired
    UserDao userDao;
    @Autowired
    SendMailService sendMailService;
    @Autowired
    Jedis jedis;
    @Override
    public ResponseInfo regist(UserRegistDto userRegistDto) {
        UserEntity user = new UserEntity();
        BeanUtils.copyProperties(userRegistDto,user);
        user.setPassword(MD5Util.MD5(user.getPassword()+user.getLoginId()));
        if(userDao.findByLoginId(user.getLoginId())!=null) return setResultFail("用户名已存在！");
        Integer insertUser = userDao.save(user, TableContains.TABLE_MB_USER);
        if (insertUser <= 0) {
            return setResultFail("注册失败！");
        }
        sendMailService.sendMail(MailTemplates.mailTemplates1(user.getEmail()));// 注册成功发送邮件
        return setResultSuccessData();
    }

    @Override
    public ResponseInfo login(UserLoginDto loginDto) {
        UserEntity user = new UserEntity();
        BeanUtils.copyProperties(loginDto,user);
        UserEntity userEntity = userDao.findByLoginIdAndPassowrd(user.getLoginId(),MD5Util.MD5(user.getPassword()+user.getLoginId()));
        if(userEntity==null){
           return setResultFail("登录失败");
        }
        String token = TokenUtil.getToken();
        userEntity.setPassword(null); // 密码不暴露
        jedis.setex(token, 60*60,JSONObject.toJSONString(userEntity)); // 将用户信息放到redis
        return setResultSuccessData(token);
    }

    @Override
    public ResponseInfo loginOut(UserOutDto outDto) {
        Long count = jedis.del(outDto.getToken());
        return setResultSuccessData(count);
    }
    @Value("${eureka.client.serviceUrl.defaultZone}")
    String url;
    @ResponseBody
    @GetMapping("test")
    public void test(){
        System.out.println("url========"+url);
    }
}
