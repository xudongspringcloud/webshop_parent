package xd.member.doapi.service.impl.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import xd.common.base.ResponseInfo;
import xd.member.doapi.dao.UserDao;
import xd.member.doapi.manager.SendMailService;

/**
 * 定时任务的控制器
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@RestController
@Slf4j
public class TaskService {
    @Autowired
    UserDao userDao;
    @Autowired
    SendMailService sendMailService;

}
