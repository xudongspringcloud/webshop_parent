package xd.member.doapi.dao;

import xd.member.doapi.entity.UserEntity;
import xd.mybatis.base.dao.BaseDao;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Administrator on 2020/2/13.
 */
@Mapper
public interface UserDao extends BaseDao {
    @Select("select  id,username,password,phone,email,createtime,updatetime from mb_user where id =#{userId}")
    UserEntity findByID(@Param("userId") Long userId);

    @Insert("INSERT  INTO `mb_user`  (username,password,phone,email,createtime,updatetime) VALUES (#{username}, #{password},#{phone},#{email},#{createtime},#{updatetime});")
    Integer insertUser(UserEntity userEntity);
    @Select("select * from mb_user where loginId=#{loginId}")
    UserEntity findByLoginId(@Param("loginId") String loginId);

    @Select("select * from mb_user where loginId=#{loginId} and password=#{password}")
    UserEntity findByLoginIdAndPassowrd(@Param("loginId") String loginId,@Param("password") String password);
    @Select("select * from mb_user where birthday=#{birthday}")
    List<UserEntity> findUserByBirthday(@Param("birthday") String birthday);
}
