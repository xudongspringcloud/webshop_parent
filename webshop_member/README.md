# 会员服务
## 1登录
使用手机和密码数据库查找对应信息
如果用户信息存在, 生成对应的token作为key,value为用户userId存放在Redis中
前台项目获取到token,存放在Cookie中，其他页面直接通过Cookie获取token、
##2 QQ互联登录
1 去开发者平台申请
https://wiki.connect.qq.com/%E6%88%90%E4%B8%BA%E5%BC%80%E5%8F%91%E8%80%85
###2.1 联合登录步骤
选择授权QQ用户,授权成功后，就会跳转到原地址

授权连接：
回调地址 ：授权成功后,跳转到回调地址
跳转到回调地址：传一些参数

跳转到回调地址：
传一个授权code有效期 10分钟  授权code使用完毕之后，直接删除，不能重复使用
授权码的作用：使用授权码换取aess_token，使用aess_token换取openid

openid作用: 唯一用户主键（授权系统会员主键，不代码腾讯userid）



openid和我们用户表中存放一个openid进行关联

使用openid调用腾讯会员接口查询QQ信息
本地回调


//步骤：
①生成授权连接，需要配置回调地址

https://graph.qq.com/oauth2.0/authorize?response_type=code&
client_id=101420900&redirect_uri=http://127.0.0.1:8764/mobile/qqLoginCallback&
state=1234656

②通过授权码换取assessToken

https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&
client_id=101420900&client_secret=bd56a336f6ac49a65005595c2a41201a&code=E28F27AFC3D8A17B75F05E9661FB933E
&redirect_uri=http://127.0.0.1:8764/mobile/qqLoginCallback	


③使用assessToken换取openid
https://graph.qq.com/oauth2.0/me?access_token=CF8775A510EA68ED8576C9F675B42862
④使用openid和assessToken获取用户信息
https://graph.qq.com/user/get_user_info?
access_token=CF8775A510EA68ED8576C9F675B42862&
oauth_consumer_key=12345&
openid=537F314752DA3A491B4F66C04D6AD9FF