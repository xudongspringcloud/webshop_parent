package xd.mq.avtive.test;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * 消费者
 */
public class Consumer {
    public static void main(String[] args) {
        //创建连接 工厂
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, "tcp://localhost:61616");
        //创建连接connect
        try {
            Connection connection = connectionFactory.createConnection();
            connection.start();
            // Session： 一个发送或接收消息的线程,false 不开启事务，
            Session session = connection.createSession(true,Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("my-queue-test1");
            MessageConsumer consumer = session.createConsumer(destination);
            while(true){
                TextMessage message = (TextMessage)consumer.receive();
                System.out.println("获取消息："+message.getText());
                session.commit();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
