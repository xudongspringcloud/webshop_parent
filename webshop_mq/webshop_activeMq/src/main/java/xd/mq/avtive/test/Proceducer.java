package xd.mq.avtive.test;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Test;
import sun.plugin.javascript.JSContext;

import javax.jms.*;

/**
 * 生产者
 */
public class Proceducer {
    // 通讯地址
    private final static String URL = "tcp://localhost:61616";
    //1 创建activemq 工厂
    private static ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER,ActiveMQConnection.DEFAULT_PASSWORD,URL);


    public static void main(String[] args) {
      //  sendsimple();
      //  sendMsg11(connectionFactory);

        sendMsg20(connectionFactory);


    }

    /**
     * 1.0 发送非事务消息
     */
    @SneakyThrows
    public static void sendsimple(){
        // 2 jms 客户端connect
        Connection connection = connectionFactory.createConnection();
        connection.start(); // start 推送
        // Session： 一个发送或接收消息的线程,false 不开启事务，
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        // Destination ：消息的目的地;消息发送给谁.
        // 获取session注意参数值my-queue是Query的名字

        Destination destination = session.createQueue("my-queue-test1");
        // MessageProducer：消息生产者
        MessageProducer producer = session.createProducer(destination);
        // 设置不持久化
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        // 发送5条消息
        for (int i = 1; i <= 5; i++) {
            // 创建一条文本消息，将字符串转化为消息
            TextMessage message = session.createTextMessage("Hello ActiveMQ！" + i);
            // 通过消息生产者发出消息
            producer.send(message);

        }
        connection.close();
    }

    /**
     * JMS 1.1
     * @param connectionFactory
     */
    public static void sendMsg11(ConnectionFactory connectionFactory){
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);// 是否开启事务，ack方式
            Destination destination = session.createQueue("my-queue-test11");
            MessageProducer producer = session.createProducer(destination);
           // producer.setDeliveryDelay(10000L); // 延时 10 秒发送
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT); // 不持久化
          //  producer.setTimeToLive(20000L); // 消息存活时间 20 秒
            TextMessage textMessage = session.createTextMessage("哈哈，我是发送者1");
            producer.send(textMessage);
            session.close();
        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * JMS 2 代码有所简化
     * @param connectionFactory
     */
    public static void sendMsg20(ConnectionFactory connectionFactory){
        try(JMSContext context = connectionFactory.createContext()){ //
            Destination destination = context.createQueue("my-queue-test20");
            ActiveMQQueue activeMQQueue = new ActiveMQQueue("");
            JMSProducer producer = context.createProducer();
            producer.send(activeMQQueue,"哈哈，我是发送者2");
        }catch (JMSRuntimeException e){
            e.printStackTrace();
        }
    }

}
