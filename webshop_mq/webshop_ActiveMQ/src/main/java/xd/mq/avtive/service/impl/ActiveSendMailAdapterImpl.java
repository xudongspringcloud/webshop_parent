package xd.mq.avtive.service.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import xd.mq.avtive.service.ActiveSendMailAdapter;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Properties;

/**
 * Created by Administrator on 2020/2/19.
 */
@Service
@Slf4j
public class ActiveSendMailAdapterImpl implements ActiveSendMailAdapter {
    /**
     * Spring Boot 提供了一个发送邮件的简单抽象，使用的是下面这个接口，这里直接注入即可使用
     */
    @Autowired
    JavaMailSenderImpl mailSender;
    @Value("${spring.mail.fromAddress}")
    private String fromAddress;
    @Override
    public void sendSimpleMail(JSONObject jsonObject) {
        String title = jsonObject.getString("title");
        String address = jsonObject.getString("address");
        String context = jsonObject.getString("context");
        //创建SimpleMailMessage对象
        SimpleMailMessage message = new SimpleMailMessage();
        //邮件发送人
        message.setFrom(fromAddress);
        //邮件接收人
        message.setTo(address);
        //邮件主题
        message.setSubject(title);
        //邮件内容
        message.setText(context);
        //发送邮件
        mailSender.send(message);
        log.info("发送邮件成功"+jsonObject.toJSONString());
    }

    @Override
    public void sendHtmlMail(JSONObject jsonObject) {
        //获取MimeMessage对象
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper;
        try {
            messageHelper = new MimeMessageHelper(message, true);
            //邮件发送人
            messageHelper.setFrom(fromAddress);
            //邮件接收人
            messageHelper.setTo(jsonObject.getString("address"));
            //邮件主题
            message.setSubject(jsonObject.getString("title"));
            //邮件内容，html格式
            messageHelper.setText(jsonObject.getString("context"), true);
            //发送
            mailSender.send(message);
            //日志信息
            log.info("邮件已经发送。");
        } catch (MessagingException e) {
            log.error("发送邮件时发生异常！", e);
        }
    }

    @Override
    public void sendAttachmentsMail(JSONObject jsonObject) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(fromAddress);
            helper.setTo(jsonObject.getString("address"));
            helper.setSubject(jsonObject.getString("title"));
            helper.setText(jsonObject.getString("context"), true);
            String filePath="";
            FileSystemResource file = new FileSystemResource(new File(filePath));
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
            helper.addAttachment(fileName, file);
            mailSender.send(message);
            //日志信息
            log.info("邮件已经发送。");
        } catch (MessagingException e) {
            log.error("发送邮件时发生异常！", e);
        }
    }
}

