package xd.mq.avtive.test.springboot;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Queue;

/**
 * Created by Administrator on 2020/2/19.
 */
@Configuration
public class QueueConfig {
    @Value("email_queue")
    String queue;
    @Bean
    public Queue create(){
        return new ActiveMQQueue(queue);
    }
}
