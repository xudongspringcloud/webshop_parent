package xd.mq.avtive.test.springboot;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2020/2/19.
 */
//@Component
public class Consumer {
    @JmsListener(destination = "${email_queue}")
    public void receive(String msg) {
        System.out.println("监听器收到msg:" + msg);
    }

}
