package xd.mq.avtive.service;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2020/2/19.
 */
public interface ActiveSendMailAdapter {
    /**
     * 发送文本邮件
     */
    public void sendSimpleMail(JSONObject jsonObject);

    /**
     * 发送HTML邮件
     */
    public void sendHtmlMail(JSONObject jsonObject);



    /**
     * 发送带附件的邮件
     */
    public void sendAttachmentsMail(JSONObject jsonObject);
}
