package xd.mq.avtive.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import xd.mq.avtive.service.ActiveSendMailAdapter;

/**
 * Created by Administrator on 2020/2/20.
 * email_queue 消费者，发送邮件
 */
@Component
@Slf4j
public class MailConsumer {
    @Autowired
    ActiveSendMailAdapter activeSendMailAdapter;
    @JmsListener(destination = "${email_queue}")
    public void sendMail(String msg){
        log.info("MailConsumer 消费 队列email_queue消息"+msg);
        JSONObject jsonObject = JSONObject.parseObject(msg);
        activeSendMailAdapter.sendSimpleMail(jsonObject);
    }
}
