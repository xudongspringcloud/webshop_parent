# 1 webshop_mobile_web
## 1.1 简介
提供web端访问功能，包括 前端页面和控制层，利用feign 调用各个其他服务。
前端模板用的 freemarker,各个包的说明如下
- controller: 控制层，控制页面跳转
- dto: 数据访问的参数类
- feign:存放调用其他服务的客户端
- model:一些模型
- redis: redis 连接的工具类
## 1.2 功能简介
前台入口项目
# 2 webshop_member
## 2.1 XXLJOB 会员生日发送邮件
   设计分布式定时任务，利用 xxljob实现
### 2.1.2 利用 XXLJOB
- 常见场景：数据同步 ，会员生日，发送祝福邮件，每天轮询，符合条件的发送邮件，一切需要定时操作的任务。
常见分布式任务的解决方案：
- 分布式锁，不推荐
- 设置开关，就是单个服务启动，还是单点系统
- 数据库主键的唯一性，不推荐
- XXLJOB 等 调度平台
###2.1.3 先搭建平台
https://www.xuxueli.com/xxl-job/

下载git 源码

执行数据库初始化脚本
部署项目
访问：http://localhost:8080/xxl-job-admin/
### 2.1.4 整合springboot+ XXLJOB
- 引入依赖
        <dependency>
            <groupId>com.xuxueli</groupId>
            <artifactId>xxl-job-core</artifactId>
            <version>2.1.2</version>
        </dependency>
- 引入 下载的配置和类
config目录下的XxlJobConfig 是配置文件，manager 目录下的 SampleXxlJob是代码。


        

